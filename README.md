# Login page  
![Login page ](raw/images/Login page.png)  
# List of customers      
![List of customers](raw/images/list.png)  

You can try this Spring application by using SQL script from sql-scripts folder.  
and modify application.properties file to your database settings  
Also you can try REST API services by using command prompt  
for GET method: `curl localhost:8080/customers -u admin:qwerty`  
for GET method by Id: `curl localhost:8080/customers/{your id} -u admin:qwerty`  
for POST method: `curl -X POST localhost:8080/customers -H "Content-type:application/json" -d {\"firstName\":\"{your name}\",\"lastName\":\"{your name}\",\"email\":\"{your email}\"} -u admin:qwerty`  
for PUT method: `curl -X PUT localhost:8080/customers -H "Content-type:application/json" -d {\"id\":\"{your id}\",\"firstName\":\"{your changed name}\",\"lastName\":\"{your changed name}\",\"email\":\"{your changed email}\"} -u admin:qwerty`  
for DELETE method: `curl -X DELETE localhost:8080/customers/{your id} -u admin:qwerty`