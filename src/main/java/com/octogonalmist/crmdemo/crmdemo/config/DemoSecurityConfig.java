package com.octogonalmist.crmdemo.crmdemo.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;

@Configuration
@EnableWebSecurity
public class DemoSecurityConfig extends WebSecurityConfigurerAdapter {
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        User.UserBuilder users = User.withDefaultPasswordEncoder();
        auth.inMemoryAuthentication().withUser(users.username("admin").password("qwerty").roles("ADMIN"));
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.httpBasic().and()
                .authorizeRequests()
                .antMatchers(HttpMethod.GET, "/customers/**").hasRole("ADMIN")
                .antMatchers(HttpMethod.POST, "/customers").hasRole("ADMIN")
                .antMatchers(HttpMethod.PUT, "/customers/**").hasRole("ADMIN")
                .antMatchers(HttpMethod.DELETE, "/customers/**").hasRole("ADMIN")
                .and().csrf().disable().formLogin().disable()
                .authorizeRequests().anyRequest().authenticated().and().formLogin()
                .loginPage("/showMyLoginPage").loginProcessingUrl("/authenticateTheUser")
                .permitAll().and().logout().permitAll();
    }
}
