package com.octogonalmist.crmdemo.crmdemo.service;

import com.octogonalmist.crmdemo.crmdemo.entity.Customer;

import java.util.List;

public interface CustomerService {
    public List<Customer> findAll();

    public Customer findById(int theId);

    public void save(Customer theCustomer);

    public void deleteById(int theId);
}
