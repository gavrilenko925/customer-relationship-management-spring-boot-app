package com.octogonalmist.crmdemo.crmdemo.dao;

import com.octogonalmist.crmdemo.crmdemo.entity.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CustomerRepository extends JpaRepository<Customer, Integer> {
    public List<Customer> findAll();
}
